package application

import (
	"fmt"
	"testing"
)

//Simple init test
func TestInit(t *testing.T) {
	err := Db.Init()
	if err != nil {
		t.Error(err.Error())
	}
}

//Tests adding and retrieving a hook
func TestAddGet(t *testing.T) {
	//Test hook
	ResetDB()
	hook := WebHook{
		WebhookURL:      "http://testurl.com:8080/",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 3.33,
		MaxTriggerValue: 3.35,
	}
	id, err := Db.AddWebHook(hook)
	if err != nil {
		t.Error(err.Error())
	}
	hook2, err2 := Db.GetWebHook(id)
	if err2 != nil {
		t.Error(err2.Error())
	}
	//omit ID from testing as it should not be the same in all cases
	hook2.ID = ""
	hook.ID = ""
	if hook2 != hook {
		t.Error("Hook's did not match. Expected equal hooks")
		fmt.Println(hook)
		fmt.Println(hook2)
	}
}

func TestInvalidGet(t *testing.T) {
	ResetDB()
	_, err := Db.GetWebHook("thisIDisNotValid")
	if err == nil {
		t.Error("Invalid id used, expected error")
	}
}

//Test getting empty hook list
func TestGetWebHooks_empty(t *testing.T) {
	ResetDB()
	hooks, err := Db.GetWebHooks()
	if err != nil {
		t.Error(err.Error())
	}
	if len(hooks) > 0 {
		t.Error("Expected empty hook list")
	}
}

func TestGetWebHooks_one(t *testing.T) {
	ResetDB()
	//Add another hook
	hook := WebHook{
		WebhookURL:      "http://testurl.com:8080/",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 3.33,
		MaxTriggerValue: 3.35,
	}
	Db.AddWebHook(hook)
	hooks, err := Db.GetWebHooks()
	if err != nil {
		t.Error(err.Error())
	}
	if len(hooks) != 1 {
		t.Error("Expected one hook")
	}
}

//add's removes and attempts to retrieve a hook
func TestRemoveWebhook(t *testing.T) {
	ResetDB()
	hook := WebHook{
		WebhookURL:      "http://testurl.com:8080/",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 3.33,
		MaxTriggerValue: 3.35,
	}
	id, _ := Db.AddWebHook(hook)
	err := Db.RemoveWebHook(id)
	if err != nil {
		t.Error(err.Error())
	}
	_, err2 := Db.GetWebHook(id)
	if err2 == nil {
		t.Error("Expected error. Webhook should be deleted")
	}
}
func TestRemoveWebhook_InvalidID(t *testing.T) {
	ResetDB()
	err := Db.RemoveWebHook("InvalidID")
	if err == nil {
		t.Error("Expected error removing invalid hook")
	}
}

//Tests updating all rates for the last 7 days and retrieving them
//Implicitly tests fetchCurrencyData
//To minimalize api query, Other functions are also tested here
func TestUpdateCurrentRates(t *testing.T) {
	ResetDB()
	err := Db.UpdateCurrencyData()
	if err != nil {
		t.Error(err.Error())
	}
	//add's test
	id, _ := Db.AddWebHook(WebHook{
		WebhookURL:      "http://testurl.com:8080/",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 3.33,
		MaxTriggerValue: 3.35,
	})

	//Tests UpdateCurrentRates
	err = Db.UpdateCurrentRates()
	if err != nil {
		t.Error(err.Error())
	}
	//AddWebHook should now have a currentRate
	hook, _ := Db.GetWebHook(id)
	if hook.CurrentRate == 0 {
		t.Error("CurrentRate should be updated.")
	}
}
