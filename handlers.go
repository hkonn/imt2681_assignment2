package application

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

//HookHandler partially copied from api_student.go
func HookHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		var wh WebHook
		//Decodes user json message
		err := json.NewDecoder(r.Body).Decode(&wh)
		if err != nil {
			http.Error(w, "Invalid json format", http.StatusBadRequest)
			return
		}

		//Adds webhook
		hookID, err2 := Db.AddWebHook(wh)
		if err2 != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		fmt.Fprint(w, hookID)
		return
		//Sends back a webhook from db with the requested id
	case "GET":
		http.Header.Add(w.Header(), "content-type", "application/json")
		//Reads id from url path
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) != 3 || len(parts[2]) != 24 {
			http.Error(w, "Malformed URL", http.StatusBadRequest)
			return
		}
		//Get's webhook with user specified id
		hook, err := Db.GetWebHook(parts[2])
		if err != nil {
			http.Error(w, "Invalid hook ID", http.StatusBadRequest)
			return
		}
		//Set's current rate to 0 to omit currentRate from response
		hook.CurrentRate = 0
		//Marshal's hook to json response
		f, err := json.Marshal(hook)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		fmt.Fprint(w, string(f))
		return
	case "DELETE":
		parts := strings.Split(r.URL.Path, "/")
		if len(parts) != 3 || len(parts[2]) != 24 {
			http.Error(w, "Malformed URL", http.StatusBadRequest)
			return
		}
		//Checks that the webhook exists
		_, err := Db.GetWebHook(parts[2])
		if err != nil {
			http.Error(w, "Invalid hook ID", http.StatusBadRequest)
			return
		}
		//Removes the webhook
		err = Db.RemoveWebHook(parts[2])
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		return
	}
}

//AvgHandler for /root/average webrequests
func AvgHandler(w http.ResponseWriter, r *http.Request) {
	var wh WebHook
	var sum, val float64
	//Decodes user json message
	err := json.NewDecoder(r.Body).Decode(&wh)
	if err != nil {
		http.Error(w, "Invalid json format", http.StatusBadRequest)
		return
	}
	//Ensure that the last 7 days of currency data exists
	err = Db.UpdateCurrencyData()
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	for i := 0; i < 7; i++ {
		val, err = Db.GetCurrencyRate(wh.BaseCurrency, wh.TargetCurrency, -i)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		sum += val
	}
	sum /= 7
	fmt.Fprint(w, sum)
}

//LatestHandler for getting the latest currency info
func LatestHandler(w http.ResponseWriter, r *http.Request) {
	var wh WebHook
	//Decodes user json message
	err := json.NewDecoder(r.Body).Decode(&wh)
	if err != nil {
		http.Error(w, "Invalid json format", http.StatusBadRequest)
		return
	}
	//Ensure that the currency data is updated
	err = Db.UpdateCurrencyData()
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	val, err2 := Db.GetCurrencyRate(wh.BaseCurrency, wh.TargetCurrency, 0)
	if err2 != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, val)
}

//TestInvoke used to invoke all hooks
func TestInvoke(w http.ResponseWriter, r *http.Request) {
	err := Db.UpdateCurrentRates()
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	err = Db.InvokeAll()
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}
