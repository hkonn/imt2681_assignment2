package application

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

//All tests in one to reduce api calls
func TestHandlers(t *testing.T) {
	//init db state
	ResetDB()
	Db.UpdateCurrencyData()
	//Make marshaled webhook
	f, _ := json.Marshal(WebHook{
		WebhookURL:      "http://testurl.com:8080/",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 3.33,
		MaxTriggerValue: 3.35,
	})
	//Create and run query
	r, err := http.NewRequest("POST", "/currency/", bytes.NewBuffer(f))
	if err != nil {
		t.Error(err.Error())
	}
	resp := httptest.NewRecorder()
	handler := http.HandlerFunc(HookHandler)
	handler.ServeHTTP(resp, r)
	//Check response for ID
	id := resp.Body.String()

	//Get the entry
	r2, err2 := http.NewRequest("GET", "/currency/"+id, nil)
	if err2 != nil {
		t.Error(err2.Error())
	}
	resp2 := httptest.NewRecorder()
	handler2 := http.HandlerFunc(HookHandler)
	handler2.ServeHTTP(resp2, r2)

	//Read response into hook
	var hook WebHook
	err = json.Unmarshal(resp2.Body.Bytes(), &hook)
	if err != nil {
		t.Error(err.Error())
	}
	if hook.WebhookURL != "http://testurl.com:8080/" {
		t.Error("Could not retrieve the hook")
	}
	//Delete the entry
	r3, err3 := http.NewRequest("DELETE", "/currency/"+id, nil)
	if err3 != nil {
		t.Error(err2.Error())
	}
	resp3 := httptest.NewRecorder()
	handler3 := http.HandlerFunc(HookHandler)
	handler3.ServeHTTP(resp3, r3)
	if resp3.Code/100 != 2 {
		t.Error("Failed to delete entry")
	}

}
