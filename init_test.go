package application

import (
	"io/ioutil"
	"os"
	"testing"

	"gopkg.in/mgo.v2/dbtest"
)

//testDb, temporary local database for testing
var testDb dbtest.DBServer

//Reset's db before each test
func ResetDB() {
	Session.DB(Db.DbName).DropDatabase()
	Db.Init()
}

//inspired / partially copied from https://github.com/mvmaasakkers/gohttptestmongodb/blob/master/init_test.go
// TestMain wraps all tests with the needed initialized mock DB and fixtures
func TestMain(m *testing.M) {
	//Set's db to testDb
	Db = &MongoDBconn{
		HookCollName:     "hooks",
		DbName:           "testDb",
		CurrencyCollName: "currencyData",
	}
	url := ":" + os.Getenv("PORT")
	hostURL = &url
	// The tempdir is created so MongoDB has a location to store its files.
	// Contents are wiped once the server stops
	tempDir, _ := ioutil.TempDir("", "testing")
	testDb.SetPath(tempDir)

	// My main session var is now set to the temporary MongoDB instance
	Session = testDb.Session()

	// Run the test suite
	retCode := m.Run()

	// Make sure we DropDatabase so we make absolutely sure nothing is left or locked while wiping the data and
	// close session
	Session.DB(Db.DbName).DropDatabase()
	Session.Close()

	// Stop shuts down the temporary server and removes data on disk.
	testDb.Stop()

	// call with result of m.Run()
	os.Exit(retCode)
}
