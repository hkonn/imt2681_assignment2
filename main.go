package main

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/hkonn/imt2681_assignment2/application"
	"gopkg.in/mgo.v2"
)

//Session mongodb session

var hostURL *string

func main() {
	//init
	var url string
	port := os.Getenv("PORT")
	if port != "" {
		url = ":" + port
	} else {
		url = ":8080"
	}
	hostURL = &url
	application.Db = &application.MongoDBconn{
		HookCollName:     "hooks",
		DbName:           "currencydb",
		DbURL:            "mongodb://localhost",
		CurrencyCollName: "currencyData",
	}
	dburl := os.Getenv("MONGODBURL")
	if dburl != "" {
		application.Db.DbURL = dburl
	}
	var err error
	application.Session, err = mgo.Dial(application.Db.DbURL)
	if err != nil {
		fmt.Printf("Error in main while opening session: %v", err.Error())
	}
	defer application.Session.Close()
	application.Db.Init()
	application.Db.UpdateCurrencyData()
	http.HandleFunc("/currency/", application.HookHandler)
	http.HandleFunc("/currency/average/", application.AvgHandler)
	http.HandleFunc("/currency/latest/", application.LatestHandler)
	http.HandleFunc("/currency/evaluationtrigger/", application.TestInvoke)
	http.ListenAndServe(*hostURL, nil)
}
