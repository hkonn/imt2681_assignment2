package application

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

//WebHook is used to store webhook information
type WebHook struct {
	ID              bson.ObjectId `json:"-" bson:"_id"`
	WebhookURL      string        `json:"webhookURL,omitempty"`
	BaseCurrency    string        `json:"baseCurrency,omitempty"`
	TargetCurrency  string        `json:"targetCurrency,omitempty"`
	CurrentRate     float64       `json:"currentRate,omitempty"`
	MinTriggerValue float64       `json:"minTriggerValue,omitempty"`
	MaxTriggerValue float64       `json:"maxTriggerValue,omitempty"`
}

//Invoke invokes the webhook to WebHook.URL
func (wh *WebHook) Invoke() error {
	//Marshal's hook to json response with omitted URL
	err := errors.New("CurrentRate not updated")
	if wh.CurrentRate == 0 {
		fmt.Printf("\nerror in Invoke(): %v", err.Error())
		return err
	}

	//tempHook for wh version with omitted url
	tempHook := *wh
	tempHook.WebhookURL = ""
	f, err := json.Marshal(tempHook)
	if err != nil {
		fmt.Printf("\nerror in Invoke(): %v", err.Error())
		return err
	}

	//If currentRate is within trigger, exit
	if wh.MaxTriggerValue > wh.CurrentRate && wh.CurrentRate > wh.MinTriggerValue {
		return nil
	}
	//Create post request
	r, err := http.NewRequest("POST", wh.WebhookURL, bytes.NewBuffer(f))
	if err != nil {
		fmt.Printf("\nerror in Invoke(): %v", err.Error())
		return err
	}
	r.Header.Set("Content-type", "application/json")

	//Do the request
	c := &http.Client{}
	re, err := c.Do(r)
	if err != nil {
		fmt.Printf("\nerror in Invoke(): %v", err.Error())
		return err
	}
	defer re.Body.Close()
	if re.StatusCode != 200 {
		return errors.New("Webhook status not ok")
	}
	return nil
}
