package application

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//Session for mongodb
var Session *mgo.Session

//Db database info
var Db *MongoDBconn
var hostURL *string

//MongoDBconn information struct
type MongoDBconn struct {
	DbURL            string
	DbName           string
	CurrencyCollName string
	HookCollName     string
}

//CurrencyData used to create and store all currency data
type currencyData struct {
	Date string             `json:"date"`
	Data map[string]float64 `json:"data"`
}

/*
Init Initialises db connection
 * code taken/inspired by GTL youtube lecture "IMT2681 Cloud Computing:MongoDB"
*/
func (db *MongoDBconn) Init() error {
	//Indexing instructions for webhooks
	HookIndex := mgo.Index{
		Key:        []string{"webhookurl"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	//Indexing instructions for currency data collection
	CurrColIndex := mgo.Index{
		Key:        []string{"date"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err := Session.DB(Db.DbName).C(Db.CurrencyCollName).EnsureIndex(CurrColIndex)
	if err != nil {
		fmt.Printf("\nerror in Init(): %v", err.Error())
		return err
	}

	err = Session.DB(Db.DbName).C(Db.HookCollName).EnsureIndex(HookIndex)
	if err != nil {
		fmt.Printf("\nerror in Init(): %v", err.Error())
		return err
	}
	return nil
}

/*
UpdateCurrencyData used to update currency data
for the last 7 days if it does not already exist
*/
func (db *MongoDBconn) UpdateCurrencyData() error {
	currency := currencyData{}

	// Updates currency data from today and 7 days back if they are not in db
	for i := 0; i < 7; i++ {
		dateIndex := getDateID(-i)
		err := Session.DB(Db.DbName).C(Db.CurrencyCollName).Find(bson.M{"date": dateIndex}).One(&currency)
		//If the currency entry for the date was not found
		if err != nil && err.Error() == "not found" {
			//Get entry from web and update db
			currencyRates, err2 := fetchCurrencyData(dateIndex)
			if err2 != nil {
				fmt.Printf("\nerror in updateCurrencyData(): %v", err2.Error())
				return err2
			}
			Db.AddCurrencyEntry(currencyData{dateIndex, currencyRates})
			//update and invoke hooks if today's rate is updated
			if i == 0 {
				Db.UpdateCurrentRates()
				Db.InvokeAll()
			}

		} else if err != nil {
			fmt.Printf("\nerror in updateCurrencyData(): %v", err.Error())
			return err
		}
	}
	return nil
}

//AddCurrencyEntry adds a single entry to currency collection
func (db *MongoDBconn) AddCurrencyEntry(c currencyData) error {

	err := Session.DB(Db.DbName).C(Db.CurrencyCollName).Insert(c)
	if err != nil {
		fmt.Printf("\nerror in AddCurrencyEntry(): %v", err.Error())
		return err
	}
	return nil
}

//getCurrencyEntry gets currency information from a day
func (db *MongoDBconn) getCurrencyEntry(date int) (currencyData, error) {
	currency := currencyData{}
	dateIndex := getDateID(date)
	//fetches currency data from today
	err := Session.DB(Db.DbName).C(Db.CurrencyCollName).Find(bson.M{"date": dateIndex}).One(&currency)
	if err != nil {
		fmt.Printf("\nerror in getCurrencyEntry(): %v", err.Error())
		return currency, err
	}
	return currency, err
}

//GetCurrencyRate returns today's rate for the targetCurrency
func (db *MongoDBconn) GetCurrencyRate(baseCurrency string, targetCurrency string, date int) (float64, error) {
	entry, err := Db.getCurrencyEntry(date)
	if err != nil {
		fmt.Printf("\nerror in GetCurrencyRate(): %v", err.Error())
		return 0, err
	}
	val, ok := entry.Data[targetCurrency]
	if !ok {
		fmt.Printf("\nerror in GetCurrencyRate(): Could not find targetCurrency)")
		return 0, errors.New("Could not find targetCurrency")
	}
	if baseCurrency != "EUR" {
		val2, ok2 := entry.Data[baseCurrency]
		if !ok2 {
			fmt.Printf("\nerror in GetCurrencyRate(): baseCurrency")
			return 0, errors.New("Could not find baseCurrency")
		}
		val /= val2
	}
	return val, nil
}

//AddWebHook registers a new webhook or overwrites existing
func (db *MongoDBconn) AddWebHook(w WebHook) (string, error) {
	//Generates a new object id if missing
	if w.ID == "" {
		w.ID = bson.NewObjectId()
	}
	err := Session.DB(Db.DbName).C(Db.HookCollName).Insert(w)
	if err != nil {
		fmt.Printf("\nerror in AddWebHook(): %v", err.Error())
		return "", err
	}
	return w.ID.Hex(), nil
}

//GetWebHook get's a webhook entry from the database
func (db *MongoDBconn) GetWebHook(id string) (WebHook, error) {
	hook := &WebHook{}

	//ensure valid objectID
	var objectID bson.ObjectId
	if bson.IsObjectIdHex(id) {
		objectID = bson.ObjectIdHex(id)
	} else {
		return *hook, errors.New("Invalid object ID")
	}
	//retrieve hook from db
	err := Session.DB(Db.DbName).C(Db.HookCollName).Find(bson.M{"_id": objectID}).One(&hook)
	if err != nil {
		fmt.Printf("\nerror in GetWebHook(): %v", err.Error())
		return *hook, err
	}
	return *hook, nil
}

//GetWebHooks returns all webhooks in db
func (db *MongoDBconn) GetWebHooks() ([]WebHook, error) {
	var hooks []WebHook

	//Get's all hooks from db
	err := Session.DB(Db.DbName).C(Db.HookCollName).Find(nil).All(&hooks)
	if err != nil {
		fmt.Printf("\nerror in GetWebHooks(): %v", err.Error())
		return hooks, err
	}
	//return hooks, nil
	return hooks, err
}

//UpdateCurrentRates updates the hook's current values in db
func (db *MongoDBconn) UpdateCurrentRates() error {
	hooks, err := Db.GetWebHooks()
	if err != nil {
		fmt.Printf("\nerror in UpdateCurrentRates(): %v", err.Error())
		return err
	}
	//Get today's currency data
	currData, err := Db.getCurrencyEntry(0)
	if err != nil {
		fmt.Printf("\nerror in UpdateCurrentRates(): %v", err.Error())
		return err
	}
	//Loop through all hooks
	for _, v := range hooks {
		//if the currency data for target currency exists
		if _, ok := currData.Data[v.TargetCurrency]; ok {
			//Delete the existing webhook
			err := Db.RemoveWebHook(v.ID.Hex())
			if err != nil {
				fmt.Printf("\nerror in UpdateCurrentRates(): %v", err.Error())
				return err
			}

			//Set the currency value
			v.CurrentRate = currData.Data[v.TargetCurrency]

			//Add the hook back to db
			_, err = Db.AddWebHook(v)
			if err != nil {
				fmt.Printf("\nerror in UpdateCurrentRates(): %v", err.Error())
				return err
			}
		}
	}
	return nil
}

//RemoveWebHook removes a webhook entry from the database
func (db *MongoDBconn) RemoveWebHook(id string) error {
	//Verify new object id
	var objectID bson.ObjectId
	if bson.IsObjectIdHex(id) {
		objectID = bson.ObjectIdHex(id)
	} else {
		return errors.New("Invalid object ID")
	}
	err := Session.DB(Db.DbName).C(Db.HookCollName).Remove(bson.M{"_id": objectID})
	if err != nil {
		fmt.Printf("\nerror in RemoveWebHook(): %v", err.Error())
		return err
	}
	return nil
}

//fetchCurrencyData fetches currency data for a date{YYYY-MM-DD}
func fetchCurrencyData(date string) (map[string]float64, error) {
	//ferch api data for the data
	resp, err := http.Get("http://api.fixer.io/" + date + "?base=EUR")
	//if data could not be fetched, return error and empty map
	if err != nil {
		fmt.Printf("error in fetchCurrencyData(): %v", err.Error())
		return map[string]float64{}, err
	}

	//read all the data to a string
	respString, err := ioutil.ReadAll(resp.Body)
	//if data could not be fetched, return error and empty map
	if err != nil {
		fmt.Printf("error in fetchCurrencyData(): %v", err.Error())
		return map[string]float64{}, err
	}

	//object for unmarshaled currency data
	type decodeStruct struct {
		Base  string             `json:"base"`
		Date  string             `json:"date"`
		Rates map[string]float64 `json:"rates"`
	}
	var result decodeStruct

	json.Unmarshal([]byte(respString), &result)
	return result.Rates, err
}

//InvokeAll invokes all webhooks
func (db *MongoDBconn) InvokeAll() error {
	//Get's all webhooks
	var isok bool
	isok = true
	hooks, err := Db.GetWebHooks()
	if err != nil {
		fmt.Printf("\nerror in InvokeAll(): %v", err.Error())
		return err
	}
	//Ensures that rates are updated
	err = Db.UpdateCurrentRates()
	if err != nil {
		fmt.Printf("\nerror in InvokeAll(): %v", err.Error())
		return err
	}
	//Invokes all hooks
	for _, v := range hooks {
		err = v.Invoke()
		if err != nil {
			fmt.Printf("\nerror in InvokeAll(): %v", err.Error())
			isok = false
		}
	}
	if !isok {
		fmt.Printf("\nerror in InvokeAll(): %v", err.Error())
		return errors.New("One or more invocations were unsucsessful")
	}
	return nil
}

//Returns the current date in db format string with offset dayDiff
func getDateID(dayDiff int) string {
	year, month, day := time.Now().AddDate(0, 0, 1*dayDiff).Date()
	dateIndex := fmt.Sprintf("%04v-%02v-%02v", year, int(month), day)
	return dateIndex
}
